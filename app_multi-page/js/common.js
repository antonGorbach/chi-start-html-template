$(function() {

	// Load SVG-sprite
	svg4everybody({});

	// Check page state on load (desktop, tablet, mobile)
	var pageState;
	if($(window).width() >= 1024) {
		pageState = 'desktop';
	}
	else if($(window).width() < 1024 && $(window).width() >= 768) {
		pageState = 'tablet'
	}
	else if($(window).width() < 768) {
		pageState = 'mobile'
	}
	// Check page state on resize (desktop, tablet, mobile)
	$(window).on('resize', function(e) {
		if($(window).width() >= 1024) {
			pageState = 'desktop';
		}
		else if($(window).width() < 1024 && $(window).width() >= 768) {
			pageState = 'tablet'
		}
		else if($(window).width() < 768) {
			pageState = 'mobile'
		}
	});

	// Call functions depending on resolution only on page state changed (example)
	(function () {
		var currentState = pageState;
		console.log("Current page state - " + currentState);
		$(window).on('resize', function(e) {
			if (currentState !== pageState) {
				currentState = pageState;
				// if/else condition here, call your functions inside conditions
				console.log("Page state changed - " + currentState);
			}
		});
	})();

	// Run page scripts only on target pages
	if ($('#main').length) {
		// Run main page function here
		console.log('You are at Main page');
	}
	if ($('#contacts').length) {
		// Run main page function here
		console.log('You are at Contacts page');
	}
});
