var gulp                = require('gulp'),
	sass                = require('gulp-sass'),
	browserSync         = require('browser-sync'),
	concat              = require('gulp-concat'),
	uglify              = require('gulp-uglify'),
	cleanCSS            = require('gulp-clean-css'),
	cleanhtml           = require('gulp-cleanhtml'),
	rename              = require('gulp-rename'),
	del                 = require('del'),
	cache               = require('gulp-cache'),
	autoprefixer        = require('gulp-autoprefixer'),
	rigger              = require('gulp-rigger'),
	sourcemaps          = require('gulp-sourcemaps'),
	notify              = require("gulp-notify"),
	svgSprite           = require('gulp-svg-sprite'),
	svgmin              = require('gulp-svgmin'),
	cheerio             = require('gulp-cheerio'),
	replace             = require('gulp-replace');

// Use this var, depending what type of site you build (one-page or multi-page)
var path = 'app_one-page/';
// var path = 'app_multi-page/';

gulp.task('html', function () {
	return gulp.src(path+'html/*.html')
		.pipe(rigger())
		.pipe(gulp.dest(path))
		.pipe(browserSync.reload({stream: true}));
});

// Sass task
gulp.task('sass', function() {
	return gulp.src(path+'sass/**/*.sass')
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
		.pipe(rename({suffix: '.min', prefix : ''}))
		.pipe(autoprefixer(['last 15 versions']))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest(path+'css'))
		.pipe(browserSync.reload({stream: true}));
});

// Js task
gulp.task('js', function() {
	return gulp.src([
		// uncomment what you need
		path+'js/libs/jquery_3.3.1/jQuery.3.3.1.js',
		path+'js/libs/svg4everybody_2.1.9/svg4everybody.js',
		// path+'js/libs/gsap_1.20.4/TweenMax.js',
		// path+'js/libs/gsap_1.20.4/CSSRulePlugin.js',
		// path+'js/libs/scrollmagic_2.0.5/ScrollMagic.js',
		// path+'js/libs/scrollmagic_2.0.5/jquery.ScrollMagic.js',
		// path+'js/libs/scrollmagic_2.0.5/animation.gsap.js',
		// path+'js/libs/scrollmagic_2.0.5/debug.addIndicators.js',
		// path+'js/libs/slick_1.9.0/slick.js',
		// path+'js/libs/scrolltoid_1.5.8/jquery.malihu.PageScroll2id.min.js',
		path+'js/common.js' // always the last
	])
		.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest(path+'js'))
		.pipe(browserSync.reload({stream: true}));
});

// SVG sprite task
gulp.task('svgSpriteBuild', function () {
	return gulp.src(path+'img/svg/*.svg')
	// minify svg
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		// remove all fill, style and stroke declarations in out shapes
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
				$('[class]').removeAttr('class');
				$('style').remove();
			},
			parserOptions: {xmlMode: true}
		}))
		// cheerio plugin create unnecessary string '&gt;', so replace it.
		.pipe(replace('&gt;', '>'))
		// build svg sprite
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: '../sprite/sprite.svg',
					render: {
						scss: {
							dest: '../../sass/helpers/_sprite.scss',
							template: path+'sass/helpers/_sprite_template.scss'
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(path+'img'));
});

// Browser-sync task
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: path
		},
		notify: false,
		open: false,
		browser: 'google chrome'
	});
});

// Watch task
gulp.task('watch', ['html', 'sass', 'js', 'svgSpriteBuild', 'browser-sync'], function() {
	gulp.watch(path+'html/**/*.html', ['html']);
	gulp.watch(path+'sass/**/*.sass', ['sass']);
	gulp.watch(path+'js/common.js', ['js']);
	gulp.watch(path+'img/svg/*.svg', ['svgSpriteBuild']);
	gulp.watch(path+'*.html', browserSync.reload);
});

// Build task
gulp.task('build', ['removebuild', 'svg', 'html', 'sass', 'js', 'svgSpriteBuild'], function() {

	var buildHtml = gulp.src([
		path+'*.html'
		])
		// .pipe(cleanhtml())
		.pipe(gulp.dest('build'));

	var buildCss = gulp.src([
		path+'css/main.min.css'
		])
		.pipe(cleanCSS())
		.pipe(gulp.dest('build/css'));

	var buildJs = gulp.src([
		path+'js/scripts.min.js'
		])
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));

	var buildFonts = gulp.src([
		path+'fonts/**/*'
		])
		.pipe(gulp.dest('build/fonts'));

	var buildImg = gulp.src([
		path+'img/**/*'
		])
		.pipe(gulp.dest('build/img'));

});

gulp.task('svg', ['svgSpriteBuild']);

gulp.task('removebuild', function() { return del.sync('build'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);